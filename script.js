
firstArr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
secondArr = ["1", "2", "3", "sea", "user", 23];
thirdArr = ["Kharkiv", "Kiev", ["Borispol", "Irpin", ["Bila Tsrekva", "Volodarka"]], "Odessa", "Lviv", "Dnieper"];


function listDOM(arr, parent = document.body) {
   if (!Array.isArray(arr)) {
      return parent.insertAdjacentHTML('beforeend', `<li>${arr}</li>`);
   } else {
      let ul = document.createElement('ul');
      let newParent = parent.appendChild(ul);
      return arr.map((elem) => {
         return listDOM(elem, newParent)
      })
   }
}

listDOM(firstArr);
listDOM(secondArr);
listDOM(thirdArr);


let i = 3;
let timer = document.getElementById('timer');
timer.innerHTML = `До очищення сторінки залишилося ${i} секунд`;
let innerTimer = setInterval(function () {
   i--;
   if (i === 0) {
      document.body.innerHTML = "";
      clearTimeout(innerTimer);
   } else {
      timer.innerHTML = `До очищення сторінки залишилося ${i} секунд`;
   }
}, 1000);

